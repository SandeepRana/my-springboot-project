/**
 * 
 */
package com.dineshonjava.sbmdb.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Dinesh.Rajput
 *
 */
@Document
public class Booking{

	@Id
	String id;
	String name;
	String development;
	String junit;
	String sonar;
	String nPE;
	String qc;
	Date entryDate;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the development
	 */
	public String getDevelopment() {
		return development;
	}

	/**
	 * @param development the development to set
	 */
	public void setDevelopment(String development) {
		this.development = development;
	}

	/**
	 * @return the junit
	 */
	public String getJunit() {
		return junit;
	}

	/**
	 * @param junit the junit to set
	 */
	public void setJunit(String junit) {
		this.junit = junit;
	}

	/**
	 * @return the sonar
	 */
	public String getSonar() {
		return sonar;
	}

	/**
	 * @param sonar the sonar to set
	 */
	public void setSonar(String sonar) {
		this.sonar = sonar;
	}

	/**
	 * @return the nPE
	 */
	public String getnPE() {
		return nPE;
	}

	/**
	 * @param nPE the nPE to set
	 */
	public void setnPE(String nPE) {
		this.nPE = nPE;
	}

	/**
	 * @return the qc
	 */
	public String getQc() {
		return qc;
	}

	/**
	 * @param qc the qc to set
	 */
	public void setQc(String qc) {
		this.qc = qc;
	}

	/**
	 * @return the entryDate
	 */
	public Date getEntryDate() {
		return entryDate;
	}

	/**
	 * @param entryDate the entryDate to set
	 */
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	
	
	
}
